
<table class="table">
    <tr>
        <th>date</th>
        <th>prize</th>
        <th>status</th>
    </tr>
    <?php foreach ($data['games'] as $game) : ?>
        <tr>
            <td><?=$game['play_date']?></td>
            <td><?=$game['prize']?></td>
            <td>
                <?=$game['status']?>
                <?php if ($game['prize_id'] == 2) : ?>
                    <br>
                    <?php if ($game['status_id'] == 1) : ?>
                    <a href="/prize/convert/<?=$game['id'] ?>" class="btn btn-primary">Convert to balance</a>
                    <a href="/prize/withdraw/<?=$game['id'] ?>" class="btn btn-danger">withdraw to bank</a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($game['prize_id'] == 1 && $game['status_id'] != 5) : ?>
                    <br>
                    <a href="/prize/refuse/<?=$game['id'] ?>" class="btn btn-primary">I don't need it.</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>