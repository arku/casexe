<html>
<head>
    <link rel="stylesheet" href="/assets/styles.css">
    <script src="/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="/assets/main.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <nav class="navbar navbar-light bg-faded">
            <a class="navbar-brand" href="/dashboard">CasExe</a>
                <div class="navbar-collapse">
                    <ul class="navbar-nav nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="/dashboard">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/game/MyGames">My games</a>
                        </li>
                    </ul>
                </div>
        </nav>