<div class="auth-form">
    <div class="loginmodal-container">
        <h1>Login to your Account</h1><br>
        <?php
        flashMessage();
        ?>
        <form method="POST">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                       aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPwd1">Password</label>
                <input type="password" class="form-control" name="pwd" id="exampleInputPwd1" aria-describedby="pwdlHelp"
                       placeholder="Enter password">
                <small id="pwdHelp" class="form-text">Your password will be secured. May be.</small>
            </div>
            <input type="submit" name="login" class="form-control btn-success" value="Log in">
        </form>

        <div class="login-help">
            <a href="/auth/register">Register</a> - <a href="/auth/forgot">Forgot Password</a>
        </div>
    </div>
</div>