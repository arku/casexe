<div class="auth-form">
    <h1 class="h1">Welcome to arku-roulette!</h1>
    <div class="desc">
        После аутентификации
        пользователь может нажать на кнопку и получить случайный приз. Призы бывают 3х
        типов: денежный (случайная сумма в интервале), бонусные баллы (случайная сумма в
        интервале), физический предмет (случайный предмет из списка).
    </div>
    <a href="/game/play" class="btn btn-primary">Play via PHP (Page will be reloaded)</a>
    <br><br> <!-- modern margin -->
    <button class="btn btn-primary" id="playajax">Play via JS Ajax (Page will NOT reload)</button>

    <div class="ajaxresult">
        <h5>Your ajax game result</h5>
        <div class="result"></div>
    </div>
</div>