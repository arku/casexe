<?php

namespace App;

class Dashboard extends MainController
{
    public function __construct()
    {
        parent::__construct();
        $this->redirectToLogin();
    }
    public function index()
    {
        $this->view->render('dashboard/index');
    }
}