<?php

namespace App;

class Prize extends MainController
{

    public function __construct()
    {
        parent::__construct();
        $this->redirectToLogin();
    }

    public function index()
    {
        $this->redirectToLogin();
    }

    public function convert($id)
    {
        $roulete = new Roulette();
        $res = $roulete->convert($id, $_SESSION['user']['id']);
        $this->redirect('/game/myGames', 'alert-success', 'your money were converted');
    }

    public function refuse($id)
    {
        $roulete = new Roulette();
        $res = $roulete->refuse($id);
        $this->redirect('/game/myGames', 'alert-success', 'you has refused your prize');
    }

    public function withdraw($id)
    {
        $roulete = new Roulette();
        $res = $roulete->withdraw($id);
        $this->redirect('/game/myGames', 'alert-success', 'your has asked for withdraw. Wait for a year');
    }
}