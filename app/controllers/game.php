<?php
namespace App;
class Game extends MainController
{

    public function __construct()
    {
        parent::__construct();
        $this->redirectToLogin();
    }
    public function index()
    {
        $this->redirect('/game/play/');
    }

    public function play()
    {
        $game = new \App\Classes\Game();
        $data['msg'] = $game->roll();
        if (strtoupper($_SERVER['REQUEST_METHOD'] == 'POST')) {
            echo json_encode($data);
        } else {
            $this->view->render('game/roll', $data);
        }
    }

    public function myGames()
    {
        $roulete = new Roulette();
        $data['games'] = $roulete->getMyGames();
        $this->view->render('game/my', $data);
    }
}