<?php

namespace App;

class Auth extends MainController
{

    public function __construct()
    {
        parent::__construct();
        if ($this->isLoggedIn()) {
            $this->redirect('/dashboard');
        }
    }
    public function login()
    {
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
            $this->doLogin();
        } else {
            $this->view->render('auth/login');
        }
    }

    private function doRegister()
    {
        $email = $this->clearData($_POST['email']);
        $name = $this->clearData($_POST['name']);
        $pwd = $this->clearData($_POST['pwd']);

        $user = new User();
        $isAlreadyRegistered = $user->isAlreadyRegistered($email);
        if ($isAlreadyRegistered) {
            $this->redirect('/auth/login', 'alert-danger', 'You are registered. Log in to continue');
        } else {
            $r=$user->registerUser($email, $name, $pwd);
            if (is_numeric($r)) {
                $_SESSION['user'] = [
                    'email' => $email,
                    'name' => $name,
                    'id' => $r
                ];
                $this->redirect('/dashboard', 'alert-success', 'You are registered.');

            }
        }
    }

    public function register()
    {
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
            $this->doRegister();
        } else {
            $this->view->render('auth/register');
        }
    }

    private function doLogin()
    {
        $email = $this->clearData($_POST['email']);
        $pwd = $this->clearData($_POST['pwd']);

        $user = new User();
        $loggedin = $user->checkLogin($email, $pwd);
        if ($loggedin) {
            $this->redirect('/dashboard');
        }
    }
}