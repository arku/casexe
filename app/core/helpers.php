<?php
function flashMessage()
{
    if (!empty($_SESSION['status'])) {
        $html = "<div class='{$_SESSION['status']}'>{$_SESSION['message']}</div>";
        unset($_SESSION['status']);
        unset($_SESSION['message']);
        echo $html;
    }
}