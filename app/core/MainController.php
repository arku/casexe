<?php
namespace App;

class MainController
{
    protected $view;
    protected $user;

    public function __construct()
    {
        $this->view = new View();
        $this->getUserdata();
    }

    public function redirect($url, $status = null, $message = null)
    {
        if (!empty($status)) {
            $_SESSION['status'] = $status;
            $_SESSION['message'] = $message;
        }
        $host  = $_SERVER['HTTP_HOST'];
        header("Location: http://$host".$url);
    }

    public function clearData($data)
    {
        return strip_tags($data);
    }


    private function getUserdata()
    {
        if (!empty($_SESSION['user'])) {
            $this->user = $_SESSION['user'];
        }
    }

    public function isLoggedIn()
    {
        return !empty($this->user);
    }

    public function redirectToLogin()
    {
        if (!$this->isLoggedIn()) {
            $this->redirect('/auth/login', 'alert-danger', 'Auth required');
        }
    }
}