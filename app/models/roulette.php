<?php

namespace App;

class Roulette extends MainModel
{
    public function savePlay($prize, $digit, $user_id = null)
    {
        $stmt = $this->db->prepare('INSERT INTO roulette (play_date, prize, digit, user_id, status)
          VALUES (:play_date, :prize, :digit, :user_id, :status)');
        $result = $stmt->execute([
            'play_date' => date('Y-m-d H:i:s'),
            'prize' => $prize,
            'digit' => $digit,
            'user_id' => !empty($user_id) ? $user_id : $_SESSION['user']['id'],
            'status' => 1, //always 1 - new status
        ]);

        return $this->db->lastInsertId();
    }

    public function getMyGames()
    {
        $stmt = $this->db->prepare('SELECT 
        play_date, roulette_prize.title AS prize, roulette_status.title AS status,
        roulette_status.id AS status_id, roulette.prize AS prize_id, roulette.id
        FROM roulette
        LEFT JOIN roulette_prize ON (roulette.prize = roulette_prize.id)
        LEFT JOIN roulette_status ON (roulette.status = roulette_status.id)
        WHERE user_id = :user_id');
        $res = $stmt->execute(['user_id' => $_SESSION['user']['id']]);
        return $stmt->fetchAll();
    }

    public function convert($id, $user_id)
    {
        $stmt = $this->db->prepare('SELECT * FROM roulette WHERE id = :id
             AND user_id = :user_id');
        $res = $stmt->execute(['id' => $id, 'user_id' => $user_id]);
        $item = $stmt->fetch();
        if ($item['status'] == 1 && $item['prize'] = 2) {
            $stmt = $this->db->prepare('UPDATE roulette SET status = 4, prize = 3 WHERE id = :id
        AND user_id = :user_id');
            $res = $stmt->execute(['id' => $id, 'user_id' =>$user_id]);
            return $res;
        }
    }
    public function refuse($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM roulette WHERE id = :id
             AND user_id = :user_id');
        $res = $stmt->execute(['id' => $id, 'user_id' => $_SESSION['user']['id']]);
        $item = $stmt->fetch();
        if ($item['status'] == 1 && $item['prize'] = 2) {
            $stmt = $this->db->prepare('UPDATE roulette SET status = 5 WHERE id = :id
        AND user_id = :user_id');
            $res = $stmt->execute(['id' => $id, 'user_id' => $_SESSION['user']['id']]);
            return $res;
        }
    }

    public function getCntByType($type)
    {
        $stmt = $this->db->prepare('SELECT COUNT(*) as cnt FROM roulette WHERE prize = :type
             AND user_id = :user_id');
        $res = $stmt->execute(['type' => $type, 'user_id' => $_SESSION['user']['id']]);
        $item = $stmt->fetch();
        return $item['cnt'];
    }

    public function withdraw($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM roulette WHERE id = :id
             AND user_id = :user_id');
        $res = $stmt->execute(['id' => $id, 'user_id' => $_SESSION['user']['id']]);
        $item = $stmt->fetch();
        if ($item['status'] == 1 && $item['prize'] = 2) {
            //do QUERY TO BANK
            $stmt = $this->db->prepare('UPDATE roulette SET status = 2 WHERE id = :id
        AND user_id = :user_id');
            $res = $stmt->execute(['id' => $id, 'user_id' => $_SESSION['user']['id']]);
            return $res;
        }
    }

    public function withdrawEveryone($limit = 500)
    {
        $stat = [];
        $stmt = $this->db->prepare('SELECT * FROM roulette WHERE prize = :prize and status = :status LIMIT :limit');
        $stmt->execute(['prize' => 2, 'status' => 1, 'limit' => $limit]); //money and not requested to pay

        $results = $stmt->fetchAll();
        foreach ($results as $result) {
            //do QUERY TO BANK
            $stmt = $this->db->prepare('UPDATE roulette SET status = 2 WHERE id = :id');
            $res = $stmt->execute(['id' => $result['id']]); //one by one, no bulk update
            $stat[] = $result['id'];
        }
        return $stat; //return ids;
    }
}
