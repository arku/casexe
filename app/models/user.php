<?php

namespace App;

class User extends MainModel
{
    public function registerUser($email, $name, $pwd)
    {
        $stmt = $this->db->prepare('INSERT INTO users (email, name, password) VALUES (:email, :name, :password)');
        $pwd = password_hash($pwd, PASSWORD_DEFAULT);
        $result = $stmt->execute(['email' => $email, 'name' => $name, 'password' => $pwd]);
        if ($result) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function isAlreadyRegistered($email)
    {
        $stmt = $this->db->prepare('SELECT COUNT(*) as cnt from users where email = :email');
        $result = $stmt->execute(['email' => $email]);
        if ($result) {
            return $stmt->fetch()['cnt'];
        } else {
            return false;
        }
    }

    public function checkLogin($email, $pwd)
    {
        $stmt = $this->db->prepare('SELECT id, name, password from users where email = :email');
        $result = $stmt->execute(['email' => $email]);
        if ($result) {
            $userdata =  $stmt->fetch();
            $is_correct = password_verify($pwd, $userdata['password']);
            if ($is_correct) {
                $_SESSION['user']['name'] = $userdata['name'];
                $_SESSION['user']['email'] = $userdata['email'];
                $_SESSION['user']['id'] = $userdata['id'];
            }
            return $is_correct;
        }
    }

    public function topup($roll)
    {
        $user_id = $_SESSION['user']['id'];
        if (empty($user_id)) {
            return false; //additional checking to prevent update everyone request.
        }
        $stmt = $this->db->prepare('UPDATE users set balance = balance + :balance WHERE id = :id');
        $result = $stmt->execute(['balance' => $roll, 'id' => $user_id]);
    }
}
