<?php
namespace App;

use PDO;

abstract class MainModel
{
    protected $db;

    public function __construct()
    {
        $this->initDatabase();
    }

    public function initDatabase()
    {
        $dsn = "mysql:host=".DBHOST.";dbname=".DBNAME.";charset=utf8";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            $this->db = new PDO($dsn, DBUSER, DBPWD, $opt);
        } catch (\Exception $e) {
            if (MODE != 'prod') {
                echo $e->getMessage()."<br>";
                die("Setup database in core/config.php");
            }
            die("We're so sorry but app is not working atm");
        }
    }
}