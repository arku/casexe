<?php

namespace App\Classes;

use App\Roulette;
use App\User;

class Game
{
    /**
     * Main function- roll up "roulete" and determine the prize with "fair" chances.
     */
    public function roll()
    {
        $roll = rand(1, 10000);
        if ($roll > 0 && $roll < 10) { //awesome prizes - less chances
            $prize = 1;
        } elseif ($roll > 10 && $roll < 1000) {
            $prize = 2; //real money prize
        } else {
            $prize = 3; //fake balance
        }

        $roulete = new Roulette();
        //limits
        $cnt = $roulete->getCntByType(1);
        if ($prize == 1 && $cnt > 2) {
            $prize = 3;
            $roll = $roll * 100;
        }
        $cnt = $roulete->getCntByType(2);
        if ($prize == 2 && $cnt > 2) {
            $prize = 3;
            $roll = $roll * 10;
        }

        $roulete->savePlay($prize, $roll);
        $result = $this->getPrize($prize, $roll);
        return $result['msg'];
    }

    private function getPrize($prize, $roll)
    {
        switch ($prize) {
            case 1:
                $darek = $this->sendDarek($roll);
                break;
            case 2:
                $darek = $this->topupBankBalance($roll);
                break;
            case 3:
            default:
                $darek = $this->topupFakeBalance($roll);
                break;
        }
        return $darek;
    }

    private function sendDarek($roll)
    {
        if ($roll < 8 || $roll = 10) {
            $darek = 'iphone '.$roll;
        } else {
            $darek = 'huawei';
        }
        $this->notifyAboutItemWinner($darek);
        return ['msg' => "Your prize: ".$darek.'. Please wait, someone will contact you to get details'];
    }

    private function topupBankBalance($roll)
    {
//        file_get_contents('/bankapi/'.$roll);
        return ['msg' => "Your prize: Your bank account has been topuped for sum: ".$roll];
    }

    private function topupFakeBalance($roll)
    {
        $user = new User();
        $user->topup($roll);
        return ['msg' => "Your prize: Your site account has been topuped for sum: ".$roll];

    }

    private function notifyAboutItemWinner($item)
    {
        //send email to someone about winner.
    }

    public function withdrawEveryone()
    {
        $roulete = new Roulette();
        $ids = $roulete->withdrawEveryone(2); // пачками по N штук.
        // Не самое интересное решение, но реальная реализация очень сильно зависит от платежной системы.
        foreach ($ids as $id) {
            echo "Game #$id asked to withdraw to bank".PHP_EOL;
        }
        if (count($ids) == 2) {
            $this->withdrawEveryone();
        }
    }
}
