<?php

namespace App;

require_once "app/core/init.php";

class Install extends MainModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function install()
    {
        $installed = 0;

        $result = $this->db->query('SHOW tables LIKE "users"');
        $exist = $result->fetch();
        if (!$exist) {
            $this->createUsers();
            $installed++;
        }
        $result = $this->db->query('SHOW tables LIKE "roulette"');
        $exist = $result->fetch();
        if (!$exist) {
            $this->createRoulette();
            $installed++;
        }
        $result = $this->db->query('SHOW tables LIKE "roulette_prize"');
        $exist = $result->fetch();
        if (!$exist) {
            $this->createRouletteprizees();
            $installed++;
        }
        $result = $this->db->query('SHOW tables LIKE "roulette_status"');
        $exist = $result->fetch();
        if (!$exist) {
            $this->createRouletteStatuses();
            $installed++;
        }
        if ($installed == 0) {
            echo "installing is not required. delete this file";
        } else {
            echo "<br><h1>Installation complete. $installed tables installed</h1>";
        }
    }

    private function createUsers()
    {
        $query = "CREATE TABLE users
        (
            id INT PRIMARY KEY AUTO_INCREMENT,
            email VARCHAR(255),
            name VARCHAR(255),
            password VARCHAR(255),
            balance INT DEFAULT 0
        );";
        $this->db->query($query);
        echo "Users table created";
    }
    private function createRoulette()
    {
        $query = "CREATE TABLE roulette
        (
            id INT PRIMARY KEY AUTO_INCREMENT,
            play_date DATETIME,
            prize INT,
            digit INT,
            user_id INT,
            prize INT
        );";
        $this->db->query($query);
        echo "Roulette table created";
    }
    private function createRouletteprizees()
    {
        $query = "CREATE TABLE roulette_prize
        (
            id INT PRIMARY KEY AUTO_INCREMENT,
            title VARCHAR(255)
        );";
        $this->db->query($query);
        $this->db->query('INSERT INTO roulette_prize (title) VALUES("mobile phone")');
        $this->db->query('INSERT INTO roulette_prize (title) VALUES("money")');
        $this->db->query('INSERT INTO roulette_prize (title) VALUES("balance")');
        echo "Roulette prizes table created";
    }
    private function createRouletteStatuses()
    {
        $query = "CREATE TABLE roulette_status
        (
            id INT PRIMARY KEY AUTO_INCREMENT,
            title VARCHAR(255)
        );";
        $this->db->query($query);
        $this->db->query('INSERT INTO roulette_status (title) VALUES("New")');
        $this->db->query('INSERT INTO roulette_status (title) VALUES("Money were sent to bank")');
        $this->db->query('INSERT INTO roulette_status (title) VALUES("Item was sent to post")');
        $this->db->query('INSERT INTO roulette_status (title) VALUES("Money were converted to balance")');
        $this->db->query('INSERT INTO roulette_status (title) VALUES("Refused to pickup")');
        echo "Roulette statuses table created";
    }
}


$install = new Install();
$install->install();