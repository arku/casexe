<?php

use App\Roulette;

require_once __DIR__ . "/../app/core/init.php";
require_once __DIR__ . "/dbModel.php";

class testConvertMoneyTest extends \PHPUnit\Framework\TestCase
{
    public function testConvertMoney()
    {
        $roulete = new Roulette();
        $dbModel = new dbModel();
        $data = $dbModel->insertRandomWinMoney();
        $this->assertTrue(is_numeric($data['user_id'])); //check insert sql
        $this->assertTrue(is_numeric($data['rid'])); //check insert sql
        $res = $roulete->convert($data['rid'], $data['user_id']);
        $newrow = $dbModel->find($data['rid']);
        $this->assertTrue($newrow['status'] == 4); //check status changed
        $this->assertTrue($newrow['prize'] == 3); //check prize changed
    }
}