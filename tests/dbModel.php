<?php

/**
 * Class dbModel
 * In real application this file would not exist but in my "framework" I have to extend from main model
 */
class dbModel extends \App\MainModel
{
    public function insertRandomWinMoney()
    {
        $user = new \App\User();
        $user_id = $user->registerUser(time().'@email.ru', 'username', 'pwd');
        $roulette = new \App\Roulette();
        $rid = $roulette->savePlay(2, 10000, $user_id);
        return [
            'user_id' => $user_id,
            'rid' => $rid
        ];
    }

    /**
     *I so worry for not having Eloquent here :(
     */
    public function find($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM roulette WHERE id = :id');
        $res = $stmt->execute(['id' => $id]);
        $item = $stmt->fetch();
        return $item;
    }
}