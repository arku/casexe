let fs = require('fs');
let sass = require('node-sass');
let chokidar = require('chokidar');
const dev_mode = true;
let watcher;
const path = './sources/scss/'

compileAll(path+ '/main.scss');

watcher = chokidar.watch(path, {
  ignored: /[\/\\]\./, persistent: true
});

watcher.on('change', function (path, stats) {
  compileAll(path);
  let date = new Date;
  console.log(path + ' changed. Recompiled at ' + date.toLocaleTimeString())
});


function compileAll(path) {
  sass.render({
    file: path,
    outputStyle: dev_mode ? 'nested' : 'compressed',
    sourceMap: dev_mode,
  }, function (error, result) { // node-style callback from v3.0.0 onwards
    if (error) {
      console.log("Line " + error.line + ': ' + error.message);
    } else {
      fs.writeFileSync("./assets/styles.css",result.css);
    }
  });
}
